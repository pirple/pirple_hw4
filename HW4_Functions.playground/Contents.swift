//Title: Add Some Time

import UIKit

/*
    Create a function called "timeAdder" that can add two time values together. For example, it should be able to add 25 hours and 3 days together.
    The function should accept 4 parameters:
    value1, label1, value2, label2
*/

/*
    value1 and value2 should accept positive integers
    label1 and label2 should accept any of the following strings: "seconds", "minutes", "hours", "days", "second", "minute", "hour", "day"
 
    Requirements:
        1. Your function should include at least one switch
        2. Your function must accept any possible combination of inputs
        3. If the inputs are valid, it should return a tuple with 2 variables inside of it: value3, and  label3. For example:
            return (5,"minutes"). The exact label you choose to return for label3 ("minutes" for example) is up to you.
        4. If the inputs are invalid or impossible, it should return false. Here are examples of impossible and invalid inputs:
            timeAdder(5,"hour",5,"minutes") // This is impossible because "hour" is singular and 5 is plural
            timeAdder(false,false,5,"minutes") // This is invalid because the first 2 arguments are not the correct types
            timeAdder({},"days",5,"minutes") // This is invalid because the first argument is the wrong type
*/


/*
    Extra Credit:
        Rather than returning an arbitrary label for label3, return the largest label that can be used with an integer value
        For example if someone calls:
            timeAdder(20,"hours",4,"hours")
            You could return: (1,"day") rather than (24,"hours")
*/



// Main function: Passing parameters by refferences
func timeAdder (_ value1 :  Int, _ label1 :  String, _ value2 :  Int, _ label2 :  String) -> (Int, String){
    // label values:
    let inputArray : [String] = ["second", "seconds", "minute", "minutes", "hour", "hours", "day", "days"];

    // validation test:
    let validate1 : Bool = validation(value1, label1, inputArray);
    let validate2 : Bool = validation(value2, label2, inputArray);
    if (!validate1 || !validate2) { // if either does not pass validation return 400 with errorMessage:
        return (400, "errorMessage: Invalid formatt. Please enter a postive value and a correct corresponding label");
    }
    
    var tempVal1 : Int = value1;
    var tempLabel1 : String = label1;
    var tempVal2 : Int = value2;
    var tempLabel2 : String = label2;
    var totalValue: Int = 0;
    var totalLabel: String = "";
    
    // convert values to the same label, lowest label will be used:
    convertValues(&tempVal1, &tempLabel1, &tempVal2, &tempLabel2, inputArray, &totalLabel, &totalValue);
    
    largestLabel(&totalValue, &totalLabel);


    return (totalValue, totalLabel); // return calculated value and label
}

// Helpler function: checking if their is a larger label that can be returned:
func largestLabel(_ value : inout Int, _ label : inout String ) {
    
    var tempValue : Int = 1;
    var newValue : Int = 0;
    switch label {
    case "second", "seconds":
        tempValue = value % 60; // checking for minutes
        if (tempValue == 0) {
            newValue = value / 60;
            value = newValue;
            if (value == 1) {
                label = "minute";
            } else {
                label = "minutes"
            }
        }
    case "minute", "minutes":
        tempValue = value % 60;
        if (tempValue == 0) {
            newValue = value / 60;
            value = newValue;
            if (value == 1) {
                label = "hour";
            } else {
                label = "hours"
            }
        }
    case "hour", "hours":
        tempValue = value % 24;
        if (tempValue == 0) {
            newValue = value / 24;
            value = newValue;
            if (value == 1) {
                label = "day";
            } else {
                label = "days"
            }
        }
    default:
        break;
    }
}






// helper function to check validation:
func validation (_ value : Int, _ label : String, _ inputArray : [String]) -> Bool {
    // making sure value is a postive Int
    if (value < 0 ){
        return false;
    }
    
    // Make sure label being inserted matches what is accepted:
    let inputCheck : Bool = inputArray.contains(label)
    if (!inputCheck) {
        return false;
    }
    
    // if value is singular then label must be singular / same with pural
    if (value == 1) {
        // check if label is singular by checking the last character of the string has a "s" or not:
        if  (label.suffix(1) == "s") {
            return false;
        }
    } else {
        if (label.suffix(1) != "s") {
            return false;
        }
    }
    return true
}

// helper function to convert values to the lowest label
// ex if labels are days and hour the days value will be converted to hours
// Passing parameters by refferences &:
func convertValues(_ value1 : inout Int, _ label1 : inout String, _ value2 : inout Int, _ label2 : inout String, _ inputArray : [String], _ totalLabel : inout String, _ totalValue : inout Int){
    
    // unwrapping inputArray
    // ranking to see which time interval is hight
    // 0:"second"  1:"seconds" 2:"minute" 3:"minutes" 4:"hour" 5:"hours" 6:"day" 7:"days"
    let value1Index : Int = inputArray.firstIndex(of: label1)!;
    let value2Index : Int = inputArray.firstIndex(of: label2)!;
    
    // compare each index to see which is smaller than call a switch case to convert
    //tuple will hold (lowest set first, highest set next)
    var toConvert : (Int, String, Int, String) = ( 0, "", 0, "");
    if (value1Index > value2Index) {
        toConvert = (value2, label2, value1, label1);
    }
    else if (value1Index < value2Index) {
        toConvert = (value1 ,label1, value2, label2);
    }
    //find the lowest and conver the highest to its new value:
    var tempValue : Int = 0;
    switch toConvert {
    case (_, "second", _, _), (_, "seconds", _, _):
        if (toConvert.3 == "second" || toConvert.3 == "seconds") {
            //time interval is the same: move to seconds
            totalValue = value1 + value2;
            totalLabel = "seconds";
        }
        else if (toConvert.3 == "minute" || toConvert.3 == "minutes") {
            tempValue = toConvert.2 * 60; // converte minutes to seconds
            totalValue = tempValue + toConvert.0;
            totalLabel = "seconds";
        }
        else if (toConvert.3 == "hour" || toConvert.3 == "hours"){
            tempValue = (toConvert.2 * 60) * 60; // converts to minutes then seconds
            totalValue = tempValue + toConvert.0;
            totalLabel = "seconds";
        }
        else if (toConvert.3 == "day" || toConvert.3 == "days") {
            tempValue = ((toConvert.2 * 24) * 60 ) * 60; // conver days to hours to minutes to seconds
            totalValue = tempValue + toConvert.0;
            totalLabel = "seconds";
        }
    case (_, "minute", _, _), (_, "minutes", _, _):
         if (toConvert.3 == "minute" || toConvert.3 == "minutes") {
            //time interval is the same: move to seconds
            totalValue = value1 + value2;
            totalLabel = "minutes";
        }
        else if (toConvert.3 == "hour" || toConvert.3 == "hours"){
            tempValue = toConvert.2 * 60; // converts to minutes
            totalValue = tempValue + toConvert.0;
            totalLabel = "minutes";
        }
        else if (toConvert.3 == "day" || toConvert.3 == "days") {
            tempValue = (toConvert.2 * 24) * 60; // conver days to hours to minutes
            totalValue = tempValue + toConvert.0;
            totalLabel = "minutes";
        }
    case (_, "hour", _, _), (_, "hours", _, _):
        if (toConvert.3 == "hour" || toConvert.3 == "hours") {
            //time interval is the same: move to seconds
            totalValue = value1 + value2;
            totalLabel = "hours";
        }
        else if (toConvert.3 == "day" || toConvert.3 == "days") {
            tempValue = toConvert.2 * 24; // conver days to hours
            totalValue = tempValue + toConvert.0;
            totalLabel = "hours";
        }
    default:
        totalValue = toConvert.2 + toConvert.0;
        totalLabel = label1; // label1 and label2 equal each other so just using label1
    }
}






//validation(1, "seconds") // for testing, remove when done.
let result = timeAdder(2, "days", 24, "hours");

if (result.0 == 400) {
    // print error message:
    print(result.1)
} else {
    print(result);
}


